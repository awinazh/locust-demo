import time
import json
import os
from locust import HttpUser, task, between


class QuickstartUser(HttpUser):
    wait_time = between(1, 2)

    def on_start(self):
        filepath=os.getenv("WORKSPACE")
        filename=os.getenv("FILENAME")
        print(os.path.join(filepath,filename))
        f = open (os.path.join(filepath,filename), "r")
        self.basket = json.loads(f.read())
        f.close()
        print("Started create basket load testing..")
        print(self.basket)

    @task
    def create_basket(self):
        response = self.client.post("/baskets", json=self.basket)
